<?php

namespace Login\LoginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="comment")
*/
class Comment
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $comment;
    
    /**
     * @var integer
     */
    private $id;


    /**
     * Set userName
     *
     * @param string $username
     * @return Comment
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get userName
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }
    
        /**
     * Set comment
     *
     * @param string $comment
     * @return Comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getUserid()
    {
        return $this->id;
    }
}
