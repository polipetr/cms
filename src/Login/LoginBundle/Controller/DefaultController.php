<?php

namespace Login\LoginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Login\LoginBundle\Entity\Users;
use Login\LoginBundle\Modals\Login;
use Login\LoginBundle\Entity\Comment;
use Login\LoginBundle\Form\PersonType;

class DefaultController extends Controller {

    public function indexAction(Request $request) {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getEntityManager();
        $repository = $em->getRepository('LoginLoginBundle:Users');
        if ($request->getMethod() == 'POST') {
            $session->clear();
            $username = $request->get('username');
            $password = sha1($request->get('password'));
            $remember = $request->get('remember');
            $user = $repository->findOneBy(array('userName' => $username, 'password' => $password));
            if ($user) {
                if ($remember == 'remember-me') {
                    $login = new Login();
                    $login->setUsername($username);
                    $login->setPassword($password);
                    $session->set('login', $login);
                }
                return $this->render('LoginLoginBundle:Default:welcome.html.twig', array('name' => $user->getFirstName()));
            } else {
                return $this->render('LoginLoginBundle:Default:login.html.twig', array('name' => 'Login Error'));
            }
        } else {
            if ($session->has('login')) {
                $login = $session->get('login');
                $username = $login->getUsername();
                $password = $login->getPassword();
                $user = $repository->findOneBy(array('userName' => $username, 'password' => $password));
                if ($user) {
                    $page = $request->get('page');
                             
                    return $this->render('LoginLoginBundle:Default:welcome.html.twig', array('name' => $user->getFirstName(),'current_page'=> $page));
                }
            }
            return $this->render('LoginLoginBundle:Default:login.html.twig');
        }
    }

    public function logoutAction(Request $request) {
        $session = $this->getRequest()->getSession();
        $session->clear();
        return $this->render('LoginLoginBundle:Default:login.html.twig');
    }
     public function commentAction(Request $request) {
        if ($request->getMethod() == 'POST') {
            $username = $request->get('username');
            $comment = $request->get('comment');
            $user = new Comment();
            $user->setUsername($username);
            $user->setComment($comment);
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($user);
            $em->flush(); 
        }
        	$repository = $this->getDoctrine()
                ->getRepository('LoginLoginBundle:Comment');
                $comments = $repository->findAll();

        return $this->render('LoginLoginBundle:Default:comment.html.twig', array(
            'comments' => $comments
        ));
      
  
    }

    
    
}