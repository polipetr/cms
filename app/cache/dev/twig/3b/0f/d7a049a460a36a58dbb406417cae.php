<?php

/* LoginLoginBundle:Default:comment.html.twig */
class __TwigTemplate_3b0fd7a049a460a36a58dbb406417cae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LoginLoginBundle:Default:index.html.twig");

        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LoginLoginBundle:Default:index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_container($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"container\" >
    <form id=\"form\" method=\"POST\" action=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("login_login_comment"), "html", null, true);
        echo "\" data-validate=\"parsley\">
         <h2>Контактна форма</h2>
        <label>Имейл</label>
        <input type=\"text\" id=\"username\" name=\"username\" class=\"input-xlarge\" data-trigger=\"change\" data-required=\"true\" data-type=\"email\"><br/>
        <label>Коментар</label>
        <textarea name=\"comment\" data-required=\"true\"></textarea><br/>
       
        <div>
            <button class=\"btn btn-primary\" type=\"submit\" >Create Account</button>
        </div>

    </form>
</div>
         
";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["comments"]) ? $context["comments"] : $this->getContext($context, "comments")));
        foreach ($context['_seq'] as $context["_key"] => $context["person"]) {
            // line 19
            echo "<table class=\"table\">
<tr>
<td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["person"]) ? $context["person"] : $this->getContext($context, "person")), "Username"), "html", null, true);
            echo "</td>
</tr>
<td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["person"]) ? $context["person"] : $this->getContext($context, "person")), "Comment"), "html", null, true);
            echo "</td>
</tr>
</table>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['person'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 27
        echo "         
 ";
    }

    public function getTemplateName()
    {
        return "LoginLoginBundle:Default:comment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 27,  64 => 23,  59 => 21,  55 => 19,  51 => 18,  34 => 4,  31 => 3,  28 => 2,);
    }
}
