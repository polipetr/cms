<?php

/* MolStoreBundle:Default:index.html.twig */
class __TwigTemplate_2879f661175fbc91ad8fa3b39c519884 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<nav class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">
  <div class=\"container\">
      <a href=\"/product/first\">Страница 1</a>
      <a href=\"/product/second\">Страница 2</a>
      <a href=\"/product/third\">Страница 3</a>
      <a href=\"/comment\">Съобщение</a>
  </div>
</nav>
";
    }

    public function getTemplateName()
    {
        return "MolStoreBundle:Default:index.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
