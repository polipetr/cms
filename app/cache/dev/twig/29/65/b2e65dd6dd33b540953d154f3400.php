<?php

/* LoginLoginBundle:Default:signup.html.twig */
class __TwigTemplate_2965b2e65dd6dd33b540953d154f3400 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LoginLoginBundle:Default:index.html.twig");

        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LoginLoginBundle:Default:index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_container($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"container\" >
    <form id=\"form\" method=\"POST\" action=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("login_login_signup"), "html", null, true);
        echo "\" data-validate=\"parsley\">
         <h2>Коментар</h2>
        <label>Потрбителско име</label>
        <input type=\"text\" id=\"username\" name=\"username\" class=\"input-xlarge\" data-trigger=\"change\" data-required=\"true\" data-type=\"email\"><br/>
        <label>Име</label>
        <input type=\"text\" name=\"firstname\" class=\"input-xlarge\" data-trigger=\"change\" data-required=\"true\"><br/>
        <label>Коментар</label>
        <textarea name=\"comments\"></textarea>
        <div>
            <button class=\"btn btn-primary\" type=\"submit\" >Коментирай</button>
        </div>

    </form>
</div>
  
 ";
    }

    public function getTemplateName()
    {
        return "LoginLoginBundle:Default:signup.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 4,  31 => 3,  28 => 2,);
    }
}
